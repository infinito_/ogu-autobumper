# OGU AUTOBUMPER

A simple and easy way to bump your threads in Ogu

## Getting started

### Requirements:    
Node;    
NPM.     

### Guide:

1. **Clone the repo**    
   `git clone https://gitlab.com/infinito_/ogu-autobumper.git` 

2. **Install the dependencies**
   `npm install`   

2. **Rename the details.json.example to details.json**    

3. **Fill the details.json**    

4. **Start the script**    
   `node .`  
    
5. **Have fun!**   

## License
This project is licensed under the [GNU Affero General Public License v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).
