import getFact from "./getFact.js";
import fs from "node:fs";

export const run = async (page, interval, thread) => {
  let count = 0;
  const fact = await getFact();
  const cookieString = await fs.readFileSync("./cookies.json");
  const cookies = JSON.parse(cookieString);
  await page.setCookie(...cookies);
  await page.goto(thread);
  console.log(fact);
  const send = async () => {
    await page.evaluate((fact) => {
      document.getElementById("message").value =
        "Auto Bumping!\nA random fact: " + fact;
      document.getElementById("quick_reply_submit").click();
    }, fact);
    count++;
    console.log(`I've bumped ${count} ${count < 1 ? "time" : "times"}!`);
  };
  send();
  setInterval(() => {
    send();
  }, interval);
};
