import axios from "axios";
export default async function getFact() {
  return await axios
    .get("https://uselessfacts.jsph.pl/random.json?language=en")
    .then((res) => {
      return res.data.text;
    });
}
