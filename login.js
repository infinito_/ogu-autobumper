import { exit } from "node:process";
import { run } from "./bump.js";
import fs from "node:fs";
export const login = async (page, username, password, interval, thread) => {
  await page.goto("https://ogu.gg/login");
  await page.evaluate(
    (username, password) => {
      document.getElementsByName("username")[1].value = username;
      document.getElementsByName("password")[1].value = password;
      document
        .getElementsByClassName(
          "btn btn-brand font-xs font-weight:600 px-5 mt-2 mb-4 btn btn-brand font-xs font-weight:600 px-5 mt-2 mb-4"
        )[0]
        .click();
    },
    username,
    password
  );
  await page.waitForNavigation();
  const error = await page.evaluate(() => {
    document.querySelector(".error > ul > li");
  });
  if (error !== undefined) {
    console.log("Username or Password are wrong.");
    exit();
  } else {
    const cookies = await page.cookies();
    await fs.writeFile(
      "./cookies.json",
      JSON.stringify(cookies, null, 2),
      (err) => {
        if (err) {
          console.log("Error while saving the cookies");
          exit();
        } else run(page, interval, thread);
      }
    );
  }
};
