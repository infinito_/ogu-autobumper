import puppeteer from "puppeteer";
import fs from "node:fs";
import details from "./details.json" assert { type: "json" };
import { run } from "./bump.js";
import { login } from "./login.js";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();

  if (fs.existsSync("./cookies.json")) {
    run(page, details.interval, details.thread);
  } else {
    login(
      page,
      details.username,
      details.password,
      details.interval,
      details.thread
    );
  }
})();
